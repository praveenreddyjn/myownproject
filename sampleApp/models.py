from django.db import models

class Employees(models.Model):
    user_id = models.IntegerField(max_length=10, blank=False)
    first_name = models.CharField(max_length=100, blank=False)
    last_name = models.CharField(max_length=100,blank=False)
    phone_number = models.CharField(max_length=20,blank=False)
    email = models.EmailField(max_length=100,blank=False)
    role = models.BooleanField(default=False)