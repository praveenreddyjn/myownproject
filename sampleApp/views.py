from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from sampleApp.models import Employees
from sampleApp.serializers import EmployeeSerializer
from rest_framework.decorators import api_view

# getting all info, insertion, delete based on the json
@api_view(['GET', 'POST', 'DELETE'])
def employee_list(request):
    if request.method == 'GET':
        employeess = Employees.objects.all()
    
        user_id = request.query_params.get('user_id', None)
        if user_id is not None:
            employeess = employeess.filter(user_id__icontains=user_id)
        
        employeess_serializer = EmployeeSerializer(employeess, many=True)
        return JsonResponse(employeess_serializer.data, safe=False)
 
    elif request.method == 'POST':
        employee_data = JSONParser().parse(request)
        employee_serializer = EmployeeSerializer(data=employee_data)
        if employee_serializer.is_valid():
            employee_serializer.save()
            return JsonResponse(employee_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(employee_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    elif request.method == 'DELETE':
        count = Employees.objects.all().delete()
        return JsonResponse({'message': '{} user_id were deleted successfully!'.format(count[0])}, status=status.HTTP_204_NO_CONTENT)
 
# getting all info, updation, delete based on the employee Id
@api_view(['GET', 'PUT', 'DELETE'])
def employee_detail(request, pk):
    try: 
        employee = Employees.objects.get(pk=pk) 
    except Employees.DoesNotExist: 
        return JsonResponse({'message': 'The employee does not exist'}, status=status.HTTP_404_NOT_FOUND) 
 
    if request.method == 'GET': 
        employee_serializer = EmployeeSerializer(employee) 
        return JsonResponse(employee_serializer.data) 
 
    elif request.method == 'PUT': 
        employee_data = JSONParser().parse(request) 
        employee_serializer = EmployeeSerializer(employee, data=employee_data) 
        if employee_serializer.is_valid(): 
            employee_serializer.save() 
            return JsonResponse(employee_serializer.data) 
        return JsonResponse(employee_serializer.errors, status=status.HTTP_400_BAD_REQUEST) 
 
    elif request.method == 'DELETE': 
        employee.delete() 
        return JsonResponse({'message': 'Employee was deleted successfully!'}, status=status.HTTP_204_NO_CONTENT)
    
# getting all comployee details         
@api_view(['GET'])
def employee_list_published(request):
    employeess = Employees.objects.filter(published=True)
        
    if request.method == 'GET': 
        employee_serializer = EmployeeSerializer(employeess, many=True)
        return JsonResponse(employee_serializer.data, safe=False)