from rest_framework import serializers 
from sampleApp.models import Employees
 
 
class EmployeeSerializer(serializers.ModelSerializer):
 
    class Meta:
        model = Employees
        fields = ('user_id',
                  'first_name',
                  'last_name',
                  'phone_number',
                  'email',
                  'role')