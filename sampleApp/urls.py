from django.urls import path
from django.conf.urls import url 
from sampleApp import views 
 
urlpatterns = [ 
    url(r'^api/employee$', views.employee_list),
    url(r'^api/employee/(?P<pk>[0-9]+)$', views.employee_detail),
    url(r'^api/employee/submitted$', views.employee_list_published)
]