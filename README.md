# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: The goal of the project is to implement an HTTP API to support a team-member management application. The team member data will be implemented in MySQL database.The application will support listing team members, adding a new team member, editing a team member, and deleting a team member.
* Version: django v3.1.3 & MySQL(5.7)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up: Install Django by adding MySQL database details
* Configuration: Configure MySQL database and django Restframework and run migration that establish connection with database
* Dependencies: python3,django,djangorestframework
* Database configuration: MySQL: For testing i have configured localhost
* How to run test: After configuration. use the python manage.py runserver (this is start server in local host in 8000 (default port))
* Deployment instructions: Run the Project using some server like unicorn
    

### Contribution guidelines ###

* Writing tests: for getting the info curl -X GET  http://127.0.0.1:8000/api/employee and post the data curl -X POST -H "Content-Type:application/json" http://127.0.0.1:8000/api/employee -d '{"userId": 1, "firstName": "David","lastName": "Jones", "phone": "+15101234567", "emailId":"test@test.com", "role": 0}' and based on fuctionality API end point changes as mentioned in urls.py and passing unique value for deletion curl -X DELETE http://127.0.0.1:8000/api/employee/1234
* Code review: The Code is 3 different function based on adding, inserting, viewing and editing.
* Other guidelines

### MySQL db schema Script ###

CREATE TABLE sample_data.`employees` (
  `id` INT(11) NOT NULL,
  `first_name` VARCHAR(100) NOT NULL,
  `last_name` VARCHAR(100) NOT NULL,
  `phone_number` VARCHAR(20) NOT NULL,
  `email` VARCHAR(150) NOT NULL,
  `role` TINYINT(2) NOT NULL,
  `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)

### Who do I talk to? ###

* Repo owner or admin: utkbansal and lalitpatel
* Other community or team contact